# K-impact
> A Google Chrome plugin that brings climate data for over 10 000 products in Kesko's online store

We want to help Kesko to give consumers data about the environmental impact of their buying behaviour. Currently customers can't easily find information about the environmental impact of goods. 

## Getting started

For easiest installation, visit the project in [Chrome Web Store](https://chrome.google.com/webstore/detail/k-impact/mgdfimhcpioegbjijgnnpaedjkgjpdeo).
Note that Chrome Web Store may have a 60 minute delay publishing projects. The  latest version is always available to install locally.

For local installation:
1. Download or clone this repository
2. In Google Chrome, open [Extensions](chrome://extensions)
3. In the upper-right corner, switch on developer mode
4. Select `Load unpacked` from top left. The folder that should be opened is `chrome-extension/src`.
5. Enable the extension normally, and it should be good to go!


## Features

This extension makes it easy to:
* See the absolute and relative climate impact of groceries
* Select more sustainable options (e.g. plant protein instead of meat)
* See a breakdown of the shopping cart's environmental impact

