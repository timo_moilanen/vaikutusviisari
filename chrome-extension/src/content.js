const CHART_COLOR_OPTIONS = {
    thisProduct: {
        //background: 'rgba(239, 103, 48, 0.9)',
        //border: 'rgba(239, 103, 48, 1)'
        background: 'rgba(4, 30, 65, 0.9)',
        border: 'rgba(4, 30, 65, 1.0)',
    },
    comparisons: {
        //background: 'rgba(239, 103, 48, 0.6)',
        //border: 'rgba(239, 103, 48, 0.8)'
        background: 'rgba(239, 103, 48, 0.9)',
        border: 'rgba(239, 103, 48, 1)'
    }
};

const PIE_CHART_COLORS = [
    "#ff6900",
    "#cc3524",
    "#823905",
    "#F98C3E",
    "#F7C6A3",
    "#716258",
    "#0f1d41",
    "#4a90e2",
    "#888888",
    "#000000",
    "#D96C5F",
    "#A49B94",
    "#666F86",
    "#4A90E2",
    "#ADD7F6",
    "#78D5D7",
    "#F7F9F9",
    "#BED8D4",
    "#A57F60"
];

function generateChartTitle(impact) { return 'Yhteensä ' + impact + ' kg CO2' }

$(document).ready(function () {
    cartImpactUpdate();
    addClimateDataToProductPage();
});

window.addEventListener("click", () => {
    addClimateDataToProductPage();
    cartImpactUpdate();
    //addBreakdownToShoppingBasket();
});


const shoppingContainerClass = 'shopping-basket-impact-container';

let shoppingCartItems = {}

function createCartImpactContainer() {
    console.log("Create cart impact div")
    $('.shopping-list-shopping-content').each(function () {
        const parentElement = $(this);
        const impactBreakdownDiv = $('<div class=' + shoppingContainerClass + '></div>');
        parentElement.prepend(impactBreakdownDiv)

        const title = $('<h3 class=department-heading>Ilmastovaikutukset</h3>')
        impactBreakdownDiv.append(title)
        const chartDiv = $('<div class=impact-breakdown-chard-container></div>')
        impactBreakdownDiv.append(title)

    })
}

function readCartContents() {
    cartProductList = []
            
    const productResultItems = $('div.shopping-list-shopping-content').find('.product-result-item')
    const productResultIds = productResultItems.toArray().map(x => x.id)
    const productIdArrays = productResultIds.flat().map(x => x.split('-'))
    const productIds = productIdArrays.map(x => x.slice(-1)).flat()
    productIds.forEach(id => cartProductList.push({'ean': id}))

    const amountDivs = productResultItems.find('span.amount')
    const amounts = []
    for (let div of amountDivs) {
        amounts.push(parseFloat(div.innerText.replace(",", ".")))
    }
    for (i in amounts) { cartProductList[i].amount = amounts[i] 
    }
    const productAmounts = {};
    cartProductList.forEach(p => { productAmounts[p.ean] = p.amount });
    return productAmounts

}

let cartStore = {}
let chartHandle = undefined

function cartImpactUpdate() {
    //console.log("Check cart impact updates")
    if (!$('.' + shoppingContainerClass).length) {
        createCartImpactContainer();
    }
    cartContents = readCartContents();

    if (JSON.stringify(cartContents) == JSON.stringify(cartStore)) {
        //No need to update
    } else {
        
        const fetchPromises = Object.keys(cartContents).map( id => fetchProductImpactData(id))
            
        Promise.all(fetchPromises).then(function(productInfos) {
            productInfos = productInfos.filter(x=>x)
            const categories = []

            productInfos.forEach(p => {
                if (p && p.impact) {
                    p.impactSum = p.impact * p.netWeight * cartContents[p.ean]
                    if (!categories.map(x=>x.category).includes(p.impactCategory)) {
                        categories.push({
                            category: p.impactCategory, 
                            displayName: p.impactCategoryName
                        })
                    }
                }
                
                
            })
            categories.forEach(c => {
                const productItems = productInfos.filter( x => x.impactCategory == c.category)
                const productImpacts = productItems.map(x => x.impactSum)
                let sum = 0
                for (var i = 0; i < productImpacts.length; i++) {
                    sum += productImpacts[i]
                }
                c.impactSum = sum.toFixed(1)
            })

            if (!chartHandle) {

                const chartCanvas = document.createElement("canvas");
                chartCanvas.class = 'breakdown-chart';
                chartHandle = createBreakdownChart(chartCanvas, categories)
                
                console.log(chartHandle)
                $("." + shoppingContainerClass).append(chartCanvas);

                let sum = 0
                for (var i = 0; i < categories.length; i++) {
                    console.log(categories[i].impactSum)
                    sum += parseFloat(categories[i].impactSum)
                }
                chartHandle.options.title.text = generateChartTitle(sum.toFixed(1));
                chartHandle.update()

            } else {
                //Update categories
                chartHandle.data.labels = categories.map(x=>x.displayName);
                chartHandle.data.datasets[0].data = categories.map(x=>x.impactSum);
                let sum = 0
                for (var i = 0; i < categories.length; i++) {
                    console.log(categories[i].impactSum)
                    sum += parseFloat(categories[i].impactSum)
                }
                chartHandle.options.title.text = generateChartTitle(sum.toFixed(1));
                chartHandle.update();
                console.log("update chart");
            }
            
            //Update local data store
            cartStore = cartContents

            //Create or update chart
            //console.log('Got categories:')
            //console.log(categories)
            //$("." + shoppingContainerClass).append(createBreakdownChart(categories));
        })
    }


    
}

/*function addBreakdownToShoppingBasket() {

    $('.shopping-list-shopping-content').each(function () {
        //TODO:päivitä muutenkin kuin kerran
        if (!$('.' + shoppingContainerClass).length) {

            const parentElement = $(this);
            const impactBreakdownDiv = $('<div class=' + shoppingContainerClass + '></div>');
            parentElement.prepend(impactBreakdownDiv)

            const title = $('<h3 class=department-heading>Ostoskorisi ilmastovaikutukset</h3>')

            impactBreakdownDiv.append( title )
            
            cartProductList = []
            
            const productResultItems = $('div.shopping-list-shopping-content').find('.product-result-item')
            const productResultIds = productResultItems.toArray().map(x => x.id)
            const productIdArrays = productResultIds.flat().map(x => x.split('-'))
            const productIds = productIdArrays.map(x => x.slice(-1)).flat()
            productIds.forEach(id => cartProductList.push({'ean': id}))

            const amountDivs = productResultItems.find('span.amount')
            const amounts = []
            for (let div of amountDivs) {
                amounts.push(parseFloat(div.innerText.replace(",", ".")))
            }
            for (i in amounts) { cartProductList[i].amount = amounts[i] 
            }
            const productAmounts = {};
            cartProductList.forEach(p => { productAmounts[p.ean] = p.amount });
            shoppingCartItems = productAmounts

            const fetchPromises = Object.keys(productAmounts).map( id => fetchProductImpactData(id))
            
            Promise.all(fetchPromises).then(function(productInfos) {
                productInfos = productInfos.filter(x=>x)
                const categories = []

                productInfos.forEach(p => {
                    if (p && p.impact) {
                        p.impactSum = p.impact * p.netWeight * productAmounts[p.ean]
                        if (!categories.map(x=>x.category).includes(p.impactCategory)) {
                            categories.push({
                                category: p.impactCategory, 
                                displayName: p.impactCategoryName
                            })
                        }
                    }
                    
                    
                })
                categories.forEach(c => {
                    const productItems = productInfos.filter( x => x.impactCategory == c.category)
                    //console.log(productItems)
                    const productImpacts = productItems.map(x => x.impactSum)
                    //console.log(productImpacts)
                    let sum = 0
                    for (var i = 0; i < productImpacts.length; i++) {
                        sum += productImpacts[i]
                    }
                    c.impactSum = sum.toFixed(1)
                })
                title.append(createBreakdownChart(categories));
                //Hae backendistä labelit
            })


        }
    })
}*/

function addClimateDataToProductPage() {
    
    $(".product-basic-details").each(function () {

        if (!$(".impact-container").length) {
            const element = $(this);
            //    const element = $(".product-basic-details");

            const findEAN = () => {
                for (const elmt of document.querySelectorAll("h4")) {
                    if (elmt.textContent.includes("EAN")) {
                        return elmt.nextSibling.innerText;
                    }
                }
            };

            let EANCode = findEAN();
            const apiResponse = fetchProductImpactData(EANCode);


            apiResponse.then(response => {
                const resultDiv = $("<div class='impact-container'></div>");
                const chartTitle = document.createElement("h3");
                const chartSubTitle = document.createElement("p");
                chartTitle.style.padding = "0";
                chartTitle.style.margin = "0";
                chartSubTitle.style.fontSize = "14px";
                chartTitle.append('Ilmastovaikutus');
                resultDiv.append(chartTitle);
                resultDiv.append(chartSubTitle);
                element.append(resultDiv);

                if (response) {
                    const impact = response.impact;
                    const explanation = () => {
                        if (response.badLabel) {
                            return ("Tuotteen ilmastovaikutus on korkea. Jos haluat säästää ilmastoa, harkitse tuotteen korvaamista ympäristöystävällisemmällä vaihtoehdolla.");
                        } else if (response.goodLabel) {
                            return ("Tuotteen ilmastovaikutus on matalampi kuin verrokkien. Valitsemalla tämän tuotteen säästät ilmastoa.");
                        } else if (impact >= 3) {
                            return ("Tuotteen ilmastovaikutus on korkea.");
                        } else {
                            return ("Tuotteen ilmastovaikutus on matala.");
                        }
                    };
                    chartSubTitle.append(explanation());
                    element.append(createChart(response));

                    $(".product-details-image-container").each(function () {
                        // const hasBadge = $(".climate-badge").length;
                        const imageElement = $(this);

                        if (!!response.goodLabel || !!response.badLabel) {
                            const badge = document.createElement("img");
                            const source = !!response.goodLabel ? chrome.extension.getURL("/assets/badge-good.png") : chrome.extension.getURL("/assets/badge-bad.png");
                            badge.src = source;
                            badge.append(" ");
                            badge.className = "climate-badge";
                            badge.setAttribute("style", "position: absolute; top: 300px; left: 0px; max-height: 25px; z-index: 1;");

                            imageElement.get(0).setAttribute("style", "position: relative;");
                            imageElement.prepend(badge);
                        }
                    });

                } else {
                    setTimeout(() => {
                        const apologyMessage = "Valitettavasti tuotteelle ei löytynyt ilmastodataa 😞";
                        chartSubTitle.append(apologyMessage);
                        resultDiv.append(chartSubTitle);
                    }, 200);
                }
            });
        }
    });
}

const fetchProductImpactData = function (productID) {
    const url = 'https://vaikutusviisari.firebaseio.com/' + productID + '.json';
    return fetch(url)
        .then(res => res.json())
        .catch(() => undefined);
};

const productSection = $(".product-search-container").get(0);

const observerConfig = {
    attributes: true,
    childList: true,
    subtree: true
};

const productDetailsOpened = function (changes) {
    changes.forEach(change => {
        if (change.addedNodes.length && change.addedNodes[0].className === "product-details") {
            addClimateDataToProductPage();
        }
    });
};

const productObserver = new MutationObserver(productDetailsOpened);

productObserver.observe(productSection, observerConfig);



const chartData = response => {
    const chartColors = CHART_COLOR_OPTIONS;
    const thisProduct = {
        displayName: 'Tämä tuote',
        impact: response.impact,
        backgroundColor: chartColors.thisProduct.background,
        borderColor: chartColors.thisProduct.borderColor
    }

    let comparisons = response.comparison.comparison;
    comparisons.forEach(obj => {
        obj.backgroundColor = chartColors.comparisons.background;
        obj.borderColor = chartColors.comparisons.border;
    })
    comparisons.push(thisProduct)
    comparisons.sort((a, b) => a.impact - b.impact)

    return {
        labels: comparisons.map(x => x.displayName),
        datasets: [{
            label: 'C02-ekvivalenttia per kg',
            data: comparisons.map(x => x.impact),
            backgroundColor: comparisons.map(x => x.backgroundColor),
            borderColor: comparisons.map(x => x.borderColor),
            borderWidth: 1
        }]
    };
}

const barChartOptions = {
    legend: {
        display: false
    },
    scales: {
        yAxes: [{
            stacked: true,
            gridLines: {
                display: false
            }
        }],
        xAxes: [{
            barPercentage: 0.4,
            display: false,
            stacked: true,
            gridLines: {
                display: false
            }
        }]
    },
    layout: {
        padding: {
            left: 15,
            right: 15
        }
    },
};

const doughnutChartOptions = {
    legend: {
        display: true
    },
    scales: {
        yAxes: [{
            stacked: true,
            display: false,
            gridLines: {
                display: false
            }
        }],
        xAxes: [{
            barPercentage: 0.4,
            display: false,
            stacked: true,
            gridLines: {
                display: false
            }
        }]
    },
    layout: {
        padding: {
            left: 15,
            right: 15
        }
    },
    title: {
        display: true,
        text: '',
        fontStyle: 'regular',
        fontSize: 14,
        position: 'bottom'
    }
}

function createChart(response) {
    const chartCanvas = document.createElement("canvas");
    chartCanvas.class = 'impact-chart';
    const ImpactBarChart = new Chart(chartCanvas, {
        type: 'horizontalBar',
        data: chartData(response),
        options: barChartOptions
    });
    Chart.defaults.global.defaultFontFamily = "'Skatta Sans', 'Gill Sans', 'Helvetica Neue', 'Helvetica', 'Arial'";
    return chartCanvas;
}


function breakdownData(products) { return {
    labels: products.map(x => x.displayName),
    datasets: [{
        backgroundColor: PIE_CHART_COLORS,
        data: products.map(x => x.impactSum)
    }]
}}

function createBreakdownChart(canvas, products) {
    const chartCanvas = document.createElement("canvas");
    chartCanvas.class = 'breakdown-chart';
    const BreakdownBarChart = new Chart(canvas, {
        type: 'doughnut',
        data: breakdownData(products), //dummyBreakdownData,
        options: doughnutChartOptions
    });
    //BreakdownChart.options.title = 'Yht. 100 CO2-kiloa';
    Chart.defaults.global.defaultFontFamily = "'Skatta Sans', 'Gill Sans', 'Helvetica Neue', 'Helvetica', 'Arial'";
    return BreakdownBarChart;
    return chartCanvas;
}