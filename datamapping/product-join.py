import os
import json

JSON_DATA_DIR = "data-out/product-json"
DATA_OUT_DIR = "data-out"

json_filenames = os.listdir(JSON_DATA_DIR)
json_filenames = list(filter(lambda n: n != '.DS_Store', json_filenames))

product_store = []

#for filename in ['product_hedelmat-ja-vihannekset_0.json']:
for filename in json_filenames:
  print(filename)
  filepath = os.path.join(JSON_DATA_DIR, filename)
  with open(filepath, 'r') as fp:
    products = json.load(fp)
  product_store.extend(products)

outpath = os.path.join(DATA_OUT_DIR, "productlist_full.json")

with open(outpath, 'w') as fp:
  json.dump(product_store, fp, indent=2, ensure_ascii=False)

print(len(product_store))