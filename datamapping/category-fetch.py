import requests
from bs4 import BeautifulSoup
import json
import os

START_URL = 'https://www.k-ruoka.fi/kauppa/joulu'
DATA_OUT_DIR = "data-out"

html = requests.get(START_URL).content

soup = BeautifulSoup(html, 'html.parser')
data = json.loads(str(soup.div['data-state']))
categories_full = data['page']['state']['searchBar']['productCategories']


full_json_path = os.path.join(DATA_OUT_DIR, "categories.json")

with open(full_json_path, 'w') as f:
  json.dump(categories_full, f, indent=2, ensure_ascii=False)

categories = []
subcategories = []
segments = []

for category in categories_full:
  categories.append(category['path'])
  for subcategory in category['subcategories']:
    subcategories.append(subcategory['path'])
    for segment in subcategory['subcategories']:
      segments.append(segment['path'])

summary_path = os.path.join(DATA_OUT_DIR, "categories_summary.txt")
  
with open(summary_path, 'w') as f:
  f.write( '\n'.join(categories) )
  f.write('\n')
  f.write( '\n'.join(subcategories) )
  f.write('\n')
  f.write( '\n'.join(segments) )

main_category_path = os.path.join(DATA_OUT_DIR, "main_category_list.json")
with open(main_category_path, 'w') as f:
  json.dump(categories, f, indent=2, ensure_ascii=False)

