import pandas as pd
import json

CATEGORY_FILE_PATH = "data-out/categories.json"
PRODUCT_FILE_PATH = 'data-out/productlist_full.json'
IMPACT_CATEGORY_MAPPING = 'manual-mapping/category_impact_mapping.xlsx'
IMPACTS_FILE = 'manual-mapping/impacts.xlsx'
EXCEPTIONS_FILE = 'manual-mapping/exceptions.xlsx'

with open(PRODUCT_FILE_PATH, 'r') as fp:
  products_json = json.load(fp)

allRelevantProductColumns = [
  'ean', 'urlSlug', 'name', 'section',
  'averageWeight', 'batchPrice', 'batchSize', 'contentSize', 
  'contentUnit', 'departmentId',  'maxSize', 
  'netWeight', 'restriction', 
  'storeId', 'visiblePrice' ]

mostRelevantProductColumns = ['ean', 'urlSlug', 'name', 'section', 'netWeight']
productColumnNames = ['ean', 'id', 'name', 'categoryId', 'netWeight']

products = pd.DataFrame(products_json)[mostRelevantProductColumns]
products.columns = productColumnNames

print( str(len(products.index)) + " products. Example:")
print(products.loc[100])
print()

with open(CATEGORY_FILE_PATH, 'r') as fp:
  categories_json = json.load(fp)

category_store = []

impact_category_mapping = pd.DataFrame(pd.read_excel(IMPACT_CATEGORY_MAPPING))
impact_category_mapping.columns = ['categoryPath', 'impactCategorySlug']

impactCategories = {}

for index, row in impact_category_mapping.iterrows():
  path = impact_category_mapping.at[index, 'categoryPath']
  impactCategory = impact_category_mapping.at[index, 'impactCategorySlug']
  impactCategories[path] = impactCategory

for category in categories_json:
  for subcategory in category['subcategories']:
    
    if subcategory['path'] in impactCategories:
      subcategoryImpact = impactCategories[subcategory['path']]
    else:
      subcategoryImpact = None

    category_store.append({
      'categoryId': subcategory['id'],
      'categoryName': subcategory['name'],
      'categoryPath': subcategory['path'],
      'categoryType': 'subcategory',
      'impactCategory': subcategoryImpact
    })
    if 'segmentIds' in subcategory:
      for segmentId in subcategory['segmentIds']:
        category_store.append({
          'categoryId': segmentId,
          'categoryName': subcategory['name'],
          'categoryPath': subcategory['path'],
          'categoryType': 'subcategory_segment_id',
          'impactCategory': subcategoryImpact
        })
    for segment in subcategory['subcategories']:

      if segment['path'] in impactCategories:
        segmentImpact = impactCategories[segment['path']]
      else:
        segmentImpact = subcategoryImpact

      for segmentId in segment['segmentIds']:
        category_store.append({
          'categoryId': segmentId,
          'categoryName': segment['name'],
          'categoryPath': segment['path'],
          'categoryType': 'segment',
          'impactCategory': segmentImpact
        })

categories = pd.DataFrame(category_store)


impacts = pd.DataFrame(pd.read_excel(IMPACTS_FILE))
impacts.columns = ['impactCategory', 'impact', 'details', 'comparisonCategory', 'displayName']
impacts = impacts[['impactCategory', 'impact', 'comparisonCategory', 'displayName']]
impacts['comparisonCategory'] = impacts['comparisonCategory'].fillna('geneerinen')

#Drop irrelevant
impacts = impacts.dropna().query("impact != 'merkityksetön'")
impacts = impacts.drop_duplicates('impactCategory')

print( str(len(categories.index)) + " categories. Example:" )
print(categories.loc[2])
print()

products_and_categories = pd.merge(products, categories, how='left', on='categoryId')
print( str(len(products_and_categories.index)) + " products joined. Example: ")
print(products_and_categories.loc[100])
print()

#Read exceptions from separate file
read_exceptions_file = True
if read_exceptions_file:
  exceptions = pd.DataFrame(pd.read_excel(EXCEPTIONS_FILE), dtype = 'str')
  exceptions = exceptions[['ean', 'Category']]
  exceptions.columns = ['ean', 'impactCategory']

  mappable_exceptions = exceptions[exceptions['impactCategory'].isin(impacts['impactCategory'])]
  unmapped_exceptions = pd.concat([exceptions, mappable_exceptions]).drop_duplicates(keep=False)
  print("WARNING!!!!!")
  print("Found unmappable categories:")
  print(unmapped_exceptions['impactCategory'].value_counts())
  print()

  products_and_exceptions = pd.merge(products, mappable_exceptions, how='outer').dropna()
  products_and_categories.update(products_and_exceptions)

categories_with_impact = pd.merge(products_and_categories, impacts, how='left')
print( str(len(categories_with_impact.index)) + " categories joined to impact. Example: ")
print(categories_with_impact.loc[24])
print()

duplicates_dropped = categories_with_impact.drop_duplicates('ean')
print( str(len(duplicates_dropped.index)) + " products without duplicates. Example: ")
print(duplicates_dropped.loc[100])
print()

debug_to_excel = False
if debug_to_excel:
  categories_with_impact.to_excel('categories_with_impact_debug.xlsx')
  products_and_exceptions.to_excel("products_and_exceptions_debug.xlsx")
  exceptions.to_excel("exceptions_debug.xlsx")
  categories.to_excel("category_debug.xlsx")
  products.to_excel("product_debug.xlsx")
  products_and_categories.to_excel("product_category_join_debug.xlsx")
  duplicates_dropped.to_excel("product_category_join_without_duplicates_debug.xlsx")

  categorisations = pd.DataFrame(products_and_categories['name'].value_counts())
  categorisations.to_excel("multiple_categorisations.xlsx")
  mappable_exceptions.to_excel("mappable_exceptions_debug.xlsx")

ean_table = duplicates_dropped[['ean', 'impact', 'impactCategory', 'netWeight', 'comparisonCategory', 'displayName']].dropna()

export_files = False
if export_files:
  ean_table.to_excel("ean_table.xlsx")
  ean_table.to_csv("ean_table.csv")

#Comparison categories

genericComparison = {
  'comparisonType': 'generic',
  'comparison': [
    {'slug': 'juures', 'displayName': 'Peruna', 'impact': 0.2},
    {'slug': 'kananmuna', 'displayName': "Kananmuna", 'impact': 2.93},
    {'slug': 'juusto', 'displayName': 'Juusto', 'impact': 9},
    {'slug': 'naudanliha', 'displayName': "Naudanliha", 'impact': 17.6}
  ]
}

proteinComparison = {
  'comparisonType': 'protein',
  'labelLimits': {
    'good': 4,
    'bad': 10
  },
  'comparison': [
    {'slug': 'soijavalmiste', 'displayName': 'Nyhtökaura', 'impact': 0.7},
    {'slug': 'kala', 'displayName': 'Kala', 'impact': 2.5},
    {'slug': 'siipikarja','displayName': "Kananliha", 'impact': 5},
    {'slug': 'porsaanliha', 'displayName': 'Sianliha', 'impact': 6.28},
    {'slug': 'naudanliha', 'displayName': "Naudanliha", 'impact': 17.6}
  ]
}

milkComparison = {
  'comparisonType': 'milk',
  'labelLimits': {
    'good': 0.7,
    'bad': 1
  },
  'comparison': [
    {'slug': 'soija_ja_kauramaito', 'displayName': 'Kauramaito', 'impact': 0.5},
    {'slug': 'maito', 'displayName': 'Maito', 'impact': 1.17},
    {'slug': 'kasvikermat', 'displayName': 'Kasvikerma', 'impact': 1.85},
    {'slug': 'kerma', 'displayName': 'Kerma', 'impact': 3.7},
    {'slug': 'juusto', 'displayName': 'Juusto', 'impact': 9}
  ]
}

sidedishComparison = {
  'comparisonType': 'sidedish',
  'labelLimits': {
    'good': 0.5,
    'bad': 2
   },
  'comparison': [
    {'slug': 'juures', 'displayName': 'Peruna', 'impact': 0.2},
    {'slug': 'pasta', 'displayName': 'Pasta', 'impact': 1.25},
    {'slug': 'riisi','displayName': "Riisi", 'impact': 3.5},
    {'slug': 'siipikarja', 'displayName': 'Kana', 'impact': 5},
    {'slug': 'juusto', 'displayName': 'Juusto', 'impact': 9}
   ]
}

icecreamComparison = {
  'comparisonType': 'icecream',
  'labelLimits': {
    'good': 1,
    'bad': 1.1
  },
  'comparison': [
    {'slug': 'kasvijaatelo', 'displayName': 'Kaurajäätelö', 'impact': 0.85},
    {'slug': 'jaatelo', 'displayName': 'Jäätelö', 'impact': 1.7},
    {'slug': 'kerma', 'displayName': 'Kerma', 'impact': 1.17},
    {'slug': 'leivonnainen', 'displayName': 'Leivos', 'impact': 2.6},
  ]
}

yogurtComparison = {
  'comparisonType': 'yogurt',
  'labelLimits': {
    'good': 1,
    'bad': 1.1
  },
  'comparison': [
    {'slug': 'kasvijogurtti', 'displayName': 'Kaurajogurtti', 'impact': 0.9},
    {'slug': 'jogurtti', 'displayName': 'Jogurtti', 'impact': 1.8},
    {'slug': 'juusto', 'displayName': 'Juusto', 'impact': 9},
  ]
}

firebase_export = True
if firebase_export:
  impacts_to_firebase = {}
  print(list(ean_table))
  for index, row in ean_table.iterrows():
    ean = ean_table.at[index, 'ean']
    impact = ean_table.at[index, 'impact']
    impactCategory = ean_table.at[index, 'impactCategory']
    impactCategoryName = ean_table.at[index, 'displayName']
    comparisonString = ean_table.at[index, 'comparisonCategory']
    netWeight = ean_table.at[index, 'netWeight']
    
    if comparisonString == 'protein':
      comparison = proteinComparison
    elif comparisonString == 'sidedish':
      comparison = sidedishComparison
    elif comparisonString == 'milk':
      comparison = milkComparison
    elif comparisonString == 'icecream':
      comparison = icecreamComparison
    elif comparisonString == 'yogurt':
      comparison = yogurtComparison
    else:
      comparison = genericComparison
    impactDict = {
      'ean': ean,
      'impact': impact,
      'impactCategory': impactCategory,
      'impactCategoryName': impactCategoryName,
      'comparison': comparison,
      'netWeight': netWeight
    }
    if 'labelLimits' in comparison:
      if impact < comparison['labelLimits']['good']:
        impactDict['goodLabel'] = True
      elif impact > comparison['labelLimits']['bad']:
        impactDict['badLabel'] = True
    impacts_to_firebase[ean] = impactDict

  with open("data_to_firebase.json", 'w') as fp:
    json.dump(impacts_to_firebase, fp, indent=2, ensure_ascii=False)

#ean_table.to_json("ean_table.json")
