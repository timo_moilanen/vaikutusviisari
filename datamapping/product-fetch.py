import requests
from bs4 import BeautifulSoup
import os
import json

URL = 'https://www.k-ruoka.fi/kr-api/product-search'
STORE_ID = 'N106'
DATA_OUT_DIR = "data-out/product-json"
BATCH_SIZE = 30

MAIN_CATEGORIES = [
  "joulu",
  "hedelmat-ja-vihannekset",
  "maito-juusto-munat-ja-rasvat",
  "leivat-keksit-ja-leivonnaiset",
  "pakasteet",
  "lihat-ja-kalat",
  "kuivat-elintarvikkeet-ja-sailykkeet",
  "valmisruoka",
  "maustaminen-ja-leivonta",
  "makeiset-ja-naposteltavat",
  "juomat",
  "lemmikit"
]
for category in MAIN_CATEGORIES:
  #category = 'hedelmat-ja-vihannekset'
  offset = 0
  stop_loop = False
  while(not stop_loop):
    print("Fetch " + category + ' with offset ' + str(offset))
    post_url = URL + '?storeId=' + STORE_ID + '&categoryId=' + category + '&offset=' + str(offset)
    response = requests.post(post_url)
    content = response.json()
    products = content['result']
    filename = 'product_' + category + '_' + str(offset) + '.json'
    filepath = os.path.join(DATA_OUT_DIR, filename)

    with open(filepath, 'w') as fp:
      json.dump(products, fp, indent = 2, ensure_ascii=False)
    offset = offset + BATCH_SIZE
    stop_loop = not products
    #stop_loop = True